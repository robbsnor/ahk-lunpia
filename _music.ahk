; Music controll

; Volume control
Numpad0 & Up::Send {Volume_Up}
Numpad0 & Down::Send {Volume_Down}

; Music control
Numpad0 & left::Send {Media_Prev}
Numpad0 & right::Send {Media_Next}

Numpad0 & NumpadEnter::Send {Media_Play_Pause}
Numpad0 & NumpadDot::Send {Volume_Mute}

; Rebind
Numpad0::Send {Numpad0}